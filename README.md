## shadow_palm_demo_ui

Shadow Palm Demo UI, Display Grid Eye, IMU and TOF Sensor Readings in an intuitive format

## Demo

![](demo.png)



## Prerequisite Python3

    - python3 -m pip install tk
    - python3 -m pip install pillow
    - python3 -m pip install pyserial
     
