unsigned long tstart = millis();
unsigned long tend = millis();
unsigned long tdiff = tend - tstart;
#define SENSOR_INTERVAL 5000 /*5 seconds*/

void setup() {
  Serial.begin(115200);
}

void generate_sensor_set1()
{

    int i, r;
    float q;


  String s = "GE#";
  for(i=0; i<64; i++){
    r=0;
    if(tdiff >= SENSOR_INTERVAL) r=100;
    s += String(r);
    s += ",";
  }

  s += "TOF#";
  //TOF1
  r=0;
  if(tdiff >= SENSOR_INTERVAL) r=100;

  s += String(r);
  s += ",";

  //TOF2
  r=0;
  if(tdiff >= SENSOR_INTERVAL) r=100;
  s += String(r);
  s += ",";


  s += "IMU#";
  
  //w
  q = 0.0;
  if(tdiff >= SENSOR_INTERVAL) q=2.0;
  s += String(q);
  s += ",";

  //x
  q = 0.0;
  if(tdiff >= SENSOR_INTERVAL) q=2.0;
  s += String(q);
  s += ",";

  //y
  q = 0.0;
  if(tdiff >= SENSOR_INTERVAL) q=2.0;
  s += String(q);
  s += ",";

  //z
  q = 0.0;
  if(tdiff >= SENSOR_INTERVAL) q=2.0;
  s += String(q);
  s += ",";
  Serial.println("");
  Serial.println(s);
  Serial.println("");
  
}

void generate_sensor_set2()
{
  int i;
  uint16_t r;
  String s = "PR#";
  for(i=0; i<60; i++){
    r=0;
    if(tdiff >= SENSOR_INTERVAL) r=100;
    s += String(r);
    s += ",";
  }
  
  Serial.println("");
  Serial.println(s);
  Serial.println("");

}

void loop() {
  tend = millis();  
  tdiff = tend - tstart;

  if(tdiff >= SENSOR_INTERVAL){
	tstart = tend;
  }

  generate_sensor_set1();
  generate_sensor_set2();
  delay(100);
}
