void setup() {
  Serial.begin(9600);
}

void generate_sensor_set1()
{

    int i, r;
  float q;


  String s = "GE#";
  for(i=0; i<64; i++){
    r = random(0, 256); /*generate between 0 to 255*/
    s += String(r);
    s += ",";
  }

  s += "TOF#";
  //TOF1
  r = random(0, 200); /*generate between 0 to 255*/
  s += String(r);
  s += ",";

  //TOF2
  r = random(0, 200); /*generate between 0 to 255*/
  s += String(r);
  s += ",";


  s += "IMU#";
  
  //w
  q = (float) (random(0, 10) / random(1, 10)); 
  s += String(q);
  s += ",";

  //x
  q =  (float) (random(0, 10) / random(1, 10)); 
  s += String(q);
  s += ",";

  //y
  q =  (float) (random(0, 10) / random(1, 10)); 
  s += String(q);
  s += ",";

  //z
  q =  (float) (random(0, 10) / random(1, 10)); 
  s += String(q);
  s += ",";
  Serial.println("");
  Serial.println(s);
  Serial.println("");
  
}

void generate_sensor_set2()
{
  int i;
  uint16_t r;
  String s = "PR#";
  for(i=0; i<60; i++){
    r = random(0, 65535); /*generate between 0 to 255*/
    s += String(r);
    s += ",";
  }
  
  Serial.println("");
  Serial.println(s);
  Serial.println("");

}

void loop() {
  generate_sensor_set1();
  generate_sensor_set2();
  delay(100);
}
