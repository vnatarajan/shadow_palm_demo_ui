'''
@Project Name   : Shadow Palm Sensor Demo UI
@Author         : Vignesh Natarajan
@Contact        : vnatarajan@uni-bielefeld.de
'''

import sys
import random
import time
# pip uninstall pyserial
# pip install pyserial
# pip3 install pyserial
# sudo pip3 install pyserial
# python2 -m pip install pyserial
import serial
import serial.tools.list_ports
import threading
import time
import queue
from datetime import datetime

INTERVAL_FE=0.0001
INTERVAL_BE=0.001

# Library Path
sys.path.append('../lib/NeoViki_UI_Python/src')
from NeoViki_UI_Python import *

DEBUG = False
serial_handle = None
tstart_be = datetime.now()
tstart_fe = datetime.now()

sensor_queue=None
start_x = 40
start_y = 40

timer_interval=1
thread_be=None
thread_ui=None
program_quit=False

class COLOR_CODE:
    def __init__(self):
        self.dist = self.DISTANCE()
        self.temp = self.TEMPERATURE()

    class DISTANCE:
        def __init__(self):
            # Red
            self.near = "#cc3232"

            # light orange
            self.mid = "#e7b416"

            # Green
            self.far = "#99c140"

    class TEMPERATURE:
        def __init__(self):
            # Sky Blue
            self.cold = "#6BBCD1"

            # light orange
            self.warm = "#FEB938"

            # Orange
            self.hot = "#FD9415"

            # Red
            self.hotter = "#E23201"


color_code = COLOR_CODE()


class UI_PARAMS:
    def __init__(self):
        self.ge = self.GE()
        self.pr = self.PR()
        self.imu = self.IMU()
        self.tof1 = self.TOF()
        self.tof2 = self.TOF()
        self.root = self.ROOT()
        self.serial = self.SERIAL()

    class ROOT:
        def __init__(self):
            self.handle = None
            self.x = 0
            self.y = 0

    class SERIAL:
        def __init__(self):
            self.x = 0
            self.y = 0
            self.handle = None

    class GE:
        def __init__(self):
            self.handle = None
            self.x = 0
            self.y = 0

    class PR:
        def __init__(self):
            self.handle = None
            self.x = 0
            self.y = 0

    class IMU:
        def __init__(self):
            self.handle = None
            self.x = 0
            self.y = 0

    class TOF:
        def __init__(self):
            self.handle = None
            self.x = 0
            self.y = 0


class SENSOR_READINGS:
    def __init__(self):
        self.ge = self.GE()
        self.tof1 = self.TOF()
        self.tof2 = self.TOF()
        self.imu = self.IMU()
        self.pr = self.PR()

    class GE:
        def __init__(self):
            self.available = False
            self.pixel = [0] * 64
            for i in range(0, 64):
                # self.pixel[i] = random.randrange(0,255)
                self.pixel[i] = 0

    class PR:
        def __init__(self):
            self.available = False
            self.pressure = [0] * 60
            for i in range(0, 60):
                # self.pressure[i] = random.randrange(0,255)
                self.pressure[i] = 0


    class TOF:
        def __init__(self):
            self.available = False
            self.distance = int(0)

    class IMU:
        def __init__(self):
            self.available = False
            self.w = float(0)
            self.x = float(0)
            self.y = float(0)
            self.z = float(0)
            self.pitch = float(0)
            self.roll = float(0)
            self.yaw = float(0)

        def generate_euler_angles(self):
            PIby2 = (math.pi / 2)

            # roll : rotation in x axis
            roll_x = float(2 * (self.w * self.x + self.y * self.z))
            roll_y = float(1 - 2 * (self.x * self.x + self.y * self.y))
            self.roll = math.degrees(math.atan2(roll_x, roll_y))

            # pitch : rotation in y axis
            pitch_var = float(2 * (self.w * self.y - self.z * self.x))
            if (abs(pitch_var) >= 1):
                # In the event of out of range -> use 90 degrees
                self.pitch = math.degrees(math.copysign(PIby2, pitch_var))
            else:
                self.pitch = math.degrees(math.asin(pitch_var))

            # yaw : rotation in z-axis
            yaw_x = 2 * (self.w * self.z + self.x * self.y)
            yaw_y = 1 - 2 * (self.y * self.y + self.z * self.z)
            self.yaw = math.degrees(math.atan2(yaw_x, yaw_y))

            self.roll = int(self.roll)
            self.pitch = int(self.pitch)
            self.yaw = int(self.yaw)


def PREPROCESS():
    global ui_params

    ui_params = UI_PARAMS()

    # Constants
    d_list_default = "  < Select Serial Port >  "
    drop_down_width = 35
    button_width = 20

    serial_port_list = [d_list_default]
    serial_port_selected = d_list_default
    serial_connected = False

    ui_params.tof1.x = start_x
    ui_params.tof1.y = start_y

    ui_params.ge.x = start_x + (280)
    ui_params.ge.y = start_y

    ui_params.tof2.x = start_x + (2*280)
    ui_params.tof2.y = start_y

    ui_params.pr.x = start_x + (3*280)
    ui_params.pr.y = start_y

    ui_params.imu.x = start_x + 250
    ui_params.imu.y = start_y + 310


def UI_LabelsDraw():
    global ui_params
    label_color_main = '#505050'
    label_color_sec = '#92A9BD'


    try:

        # TOF1 Label
        l = label(ui_params.root.handle)
        l.width = 6
        l.height = 1
        l.color.bg = 'white'
        l.color.fg = label_color_main
        l.font.name = 'lucida'
        l.font.size = 13
        l.font.bold = True
        l.write("ToF1")
        x = ui_params.tof1.x + 110
        y = ui_params.tof1.y - 25
        l.gotoxy(x, y)

        # TOF2 Label
        l = label(ui_params.root.handle)
        l.width = 6
        l.height = 1
        l.color.bg = 'white'
        l.color.fg = label_color_main
        l.font.name = 'lucida'
        l.font.size = 13
        l.font.bold = True
        l.write("ToF2")
        x = ui_params.tof2.x + 110
        y = ui_params.tof2.y - 25
        l.gotoxy(x, y)

        # Grid Eye Label

        l = label(ui_params.root.handle)
        l.width = 6
        l.height = 1
        l.color.bg = 'white'
        l.color.fg = label_color_main
        l.font.name = 'lucida'
        l.font.size = 13
        l.font.bold = True
        l.write("GridEye")
        x = ui_params.ge.x + 100
        y = ui_params.ge.y - 25
        l.gotoxy(x, y)

        # Pressure Label

        l = label(ui_params.root.handle)
        l.width = 10
        l.height = 1
        l.color.bg = 'white'
        l.color.fg = label_color_main
        l.font.name = 'lucida'
        l.font.size = 13
        l.font.bold = True
        l.write("Pressure")
        x = ui_params.pr.x + 100
        y = ui_params.pr.y - 25
        l.gotoxy(x, y)

        # IMU Label

        l = label(ui_params.root.handle)
        l.width = 15
        l.height = 1
        l.color.bg = 'white'
        l.color.fg = label_color_main
        l.font.name = 'lucida'
        l.font.size = 13
        l.font.bold = True
        l.write("IMU Readings")
        x = ui_params.imu.x + 210
        y = ui_params.imu.y - 30
        l.gotoxy(x, y)

        l = label(ui_params.root.handle)
        l.width = 6
        l.height = 1
        l.color.bg = 'white'
        l.color.fg = label_color_sec
        l.font.name = 'lucida'
        l.font.size = 13
        l.font.bold = True
        l.write("Roll (X)")
        x = ui_params.imu.x + 80
        y = ui_params.imu.y + 220
        l.gotoxy(x, y)

        l = label(ui_params.root.handle)
        l.width = 6
        l.height = 1
        l.color.bg = 'white'
        l.color.fg = label_color_sec
        l.font.name = 'lucida'
        l.font.size = 13
        l.font.bold = True
        l.write("Pitch (Y)")
        x = ui_params.imu.x + 250
        y = ui_params.imu.y + 220
        l.gotoxy(x, y)

        l = label(ui_params.root.handle)
        l.width = 6
        l.height = 1
        l.color.bg = 'white'
        l.color.fg = label_color_sec
        l.font.name = 'lucida'
        l.font.size = 13
        l.font.bold = True
        l.write("Yaw (Z)")
        x = ui_params.imu.x + 410
        y = ui_params.imu.y + 220
        l.gotoxy(x, y)

    except:
        pass
    
def UI_GridEyeDraw(sensor_readings):
    global ui_params

    grid_width = 30
    grid_height = 30
    n_grid_rows = 8
    n_grid_cols = 8

    canvas_height = n_grid_rows * grid_height + 15
    canvas_width = n_grid_cols * grid_width + 15

    # Return if sensor data is not available
    if sensor_readings.ge.available == False:
        return

    try:
        # Delete Previous Canvas Before Updating
        if ui_params.ge.handle != None:
            ui_params.ge.handle.destroy()
            ui_params.ge.handle = None

        ui_params.ge.handle = canvas(ui_params.root.handle)
        ui_params.ge.handle.height = canvas_height
        ui_params.ge.handle.width = canvas_width
        ui_params.ge.handle.gotoxy(ui_params.ge.x, ui_params.ge.y)
        ui_params.ge.handle.color.bg = 'black'

        x = 0
        y = 0

        offset = 25
        # Display Pixels
        for r in range(0, 8):
            y = r * grid_height + offset
            for c in range(0, 8):
                x = c * grid_width + offset
                value = sensor_readings.ge.pixel[(r + 1) * c]

                if (value >= 0) and (value < 50):
                    alert_color = color_code.temp.cold
                elif (value >= 50) and (value < 100):
                    alert_color = color_code.temp.cold
                elif (value >= 100) and (value < 150):
                    alert_color = color_code.temp.warm
                elif (value >= 150) and (value < 200):
                    alert_color = color_code.temp.hot
                elif (value >= 200):
                    alert_color = color_code.temp.hotter

                b = box(ui_params.ge.handle)
                b.width = grid_width
                b.height = grid_height
                b.border.color = 'grey'
                b.border.thickness = 1
                b.color.fg = alert_color
                b.gotoxy(x, y)

                t = text(ui_params.ge.handle)
                t.font.name = 'lucida'
                t.font.size = 9
                t.value = str(value)
                t.color.fg = t.color.complement(alert_color)
                t.gotoxy(x, y)
    except:
        pass
        
    sensor_readings.ge.available = False

def UI_PRDraw(sensor_readings):
    global ui_params

    grid_width = 30
    grid_height = 30
    n_grid_rows = 8
    n_grid_cols = 8

    canvas_height = n_grid_rows * grid_height + 15
    canvas_width = n_grid_cols * grid_width + 15

    # Return if sensor data is not available
    if sensor_readings.pr.available == False:
        return

    try:
        # Delete Previous Canvas Before Updating
        if ui_params.pr.handle != None:
            ui_params.pr.handle.destroy()
            ui_params.pr.handle = None

        ui_params.pr.handle = canvas(ui_params.root.handle)
        ui_params.pr.handle.height = canvas_height
        ui_params.pr.handle.width = canvas_width
        ui_params.pr.handle.gotoxy(ui_params.pr.x, ui_params.pr.y)
        ui_params.pr.handle.color.bg = 'black'

        x = 0
        y = 0

        offset = 25
        ctr=-1
        index=0
        # Display Pixels
        for r in range(0, 8):
            y = r * grid_height + offset
            for c in range(0, 8):
                x = c * grid_width + offset
                ctr = ctr + 1
           
                if (ctr == 0) or (ctr == 7) or (ctr == 56) or (ctr == 63):
                    value = -1
                else:
                    value = sensor_readings.pr.pressure[index]
                    index = index + 1
                

                if value == -1:
                    alert_color = '#000000'
                if (value >= 0) and (value < 5000):
                    alert_color = color_code.temp.cold
                elif (value >= 5000) and (value < 10000):
                    alert_color = color_code.temp.cold
                elif (value >= 10000) and (value < 15000):
                    alert_color = color_code.temp.warm
                elif (value >= 15000) and (value < 20000):
                    alert_color = color_code.temp.hot
                elif (value >= 20000):
                    alert_color = color_code.temp.hotter

                b = box(ui_params.pr.handle)
                b.width = grid_width
                b.height = grid_height
                b.border.color = 'grey'
                b.border.thickness = 1
                b.color.fg = alert_color
                b.gotoxy(x, y)

                t = text(ui_params.pr.handle)
                t.font.name = 'lucida'
                t.font.size = 7

                if value == -1:
                    t.value = ""
                else:
                    t.value = str(value)

                t.color.fg = t.color.complement(alert_color)
                t.gotoxy(x, y)
    except:
        pass
        
    sensor_readings.pr.available = False



def UI_Tof1Draw(sensor_readings):
    global ui_params

    # Return if sensor data is not available
    if sensor_readings.tof1.available == False:
        return

    try:

        # Delete Previous Canvas Before Updating
        if ui_params.tof1.handle != None:
            ui_params.tof1.handle.destroy()
            ui_params.tof1.handle = None

        ui_params.tof1.handle = canvas(ui_params.root.handle)
        ui_params.tof1.handle.height = 255
        ui_params.tof1.handle.width = 255
        ui_params.tof1.handle.gotoxy(ui_params.tof1.x, ui_params.tof1.y)
        ui_params.tof1.handle.color.bg = 'grey'

        x = 0
        y = 0
        alert_color = color_code.dist.far
        radius_max = 90
        # Draw an Oval in the canvas
        if (sensor_readings.tof1.distance >= 0) and (sensor_readings.tof1.distance < 50):
            alert_color = color_code.dist.near
            radius = radius_max
        elif (sensor_readings.tof1.distance >= 50) and (sensor_readings.tof1.distance < 100):
            alert_color = color_code.dist.mid
            radius = radius_max - 15
        elif (sensor_readings.tof1.distance >= 100):
            alert_color = color_code.dist.far
            radius = radius_max - 30

        c = circle(ui_params.tof1.handle)
        c.radius = int(radius)
        c.border.color = 'black'
        c.border.thickness = 2
        c.color.fg = alert_color
        c.gotoxy(125, 125)

        t = text(ui_params.tof1.handle)
        t.value = str(sensor_readings.tof1.distance)
        t.color.fg = t.color.complement(alert_color)
        t.font.name = 'lucida'
        t.font.size = 40
        t.gotoxy(125, 125)
        
    except:
        pass
        
    sensor_readings.tof1.available = False

def UI_Tof2Draw(sensor_readings):
    global ui_params

    # Return if sensor data is not available
    if sensor_readings.tof2.available == False:
        return

    try:
        
        # Delete Previous Canvas Before Updating
        if ui_params.tof2.handle != None:
            ui_params.tof2.handle.destroy()
            ui_params.tof2.handle = None

        ui_params.tof2.handle = canvas(ui_params.root.handle)
        ui_params.tof2.handle.height = 255
        ui_params.tof2.handle.width = 255
        ui_params.tof2.handle.gotoxy(ui_params.tof2.x, ui_params.tof2.y)
        ui_params.tof2.handle.color.bg = 'grey'

        x = 0
        y = 0
        alert_color = color_code.dist.far
        radius_max = 90
        # Draw an Oval in the canvas
        if (sensor_readings.tof2.distance >= 0) and (sensor_readings.tof2.distance < 50):
            alert_color = color_code.dist.near
            radius = radius_max
        elif (sensor_readings.tof2.distance >= 50) and (sensor_readings.tof2.distance < 100):
            alert_color = color_code.dist.mid
            radius = radius_max - 15
        elif (sensor_readings.tof2.distance >= 100):
            alert_color = color_code.dist.far
            radius = radius_max - 30

        c = circle(ui_params.tof2.handle)
        c.radius = int(radius)
        c.border.color = 'black'
        c.border.thickness = 2
        c.color.fg = alert_color
        c.gotoxy(125, 125)

        t = text(ui_params.tof2.handle)
        t.value = str(sensor_readings.tof2.distance)
        t.color.fg = t.color.complement(alert_color)
        t.font.name = 'lucida'
        t.font.size = 40
        t.gotoxy(125, 125)
    
    except:
        pass
        
    sensor_readings.tof2.available = False


def get_degree():
    return random.randint(-360, 360)


def UI_IMUDraw(sensor_readings):
    global ui_params
    # Return if sensor data is not available
    if sensor_readings.imu.available == False:
        return
        
    try:
        
        # Delete Previous Canvas Before Updating
        if ui_params.imu.handle != None:
            ui_params.imu.handle.destroy()
            ui_params.imu.handle = None

        ui_params.imu.handle = canvas(ui_params.root.handle)
        ui_params.imu.handle.height = 205
        ui_params.imu.handle.width = 540
        ui_params.imu.handle.gotoxy(ui_params.imu.x, ui_params.imu.y)
        ui_params.imu.handle.color.bg = 'grey'

        # Simulated Data
        # sensor_readings.imu.pitch = get_degree()
        # sensor_readings.imu.roll = get_degree()
        # sensor_readings.imu.yaw = get_degree()

        x = 95
        y = 100
        alert_color = color_code.dist.far

        x_offset = 180

        b1 = box(ui_params.imu.handle)
        b1.width = 100
        b1.height = 100
        b1.border.color = 'black'
        b1.border.thickness = 2
        b1.color.fg = color_code.dist.far
        b1.gotoxy(x, y)
        b1.rotate(sensor_readings.imu.roll)

        t = text(ui_params.imu.handle)
        t.value = str(sensor_readings.imu.roll)
        t.color.fg = t.color.complement(color_code.dist.far)
        t.font.name = 'lucida'
        t.font.size = 20
        t.gotoxy(x, y)

        b2 = box(ui_params.imu.handle)
        b2.width = 100
        b2.height = 100
        b2.border.color = 'black'
        b2.border.thickness = 2
        b2.color.fg = color_code.dist.mid
        b2.gotoxy(x + x_offset, y)
        b2.rotate(sensor_readings.imu.pitch)

        t = text(ui_params.imu.handle)
        t.value = str(sensor_readings.imu.pitch)
        t.color.fg = t.color.complement(color_code.dist.mid)
        t.font.name = 'lucida'
        t.font.size = 20
        t.gotoxy(x + x_offset, y)

        b3 = box(ui_params.imu.handle)
        b3.width = 100
        b3.height = 100
        b3.border.color = 'black'
        b3.border.thickness = 2
        b3.color.fg = color_code.dist.near
        b3.gotoxy(x + x_offset + x_offset, y)
        b3.rotate(sensor_readings.imu.yaw)

        t = text(ui_params.imu.handle)
        t.value = str(sensor_readings.imu.yaw)
        t.color.fg = t.color.complement(color_code.dist.near)
        t.font.name = 'lucida'
        t.font.size = 20
        t.gotoxy(x + x_offset + x_offset, y)
    
    except:
        pass
        
    sensor_readings.imu.available = False

def Frontend_Routine():
    global ui_params

    PREPROCESS()

    ui_params.root.handle = BEGIN()
    ui_params.root.handle.title = "ShadowPalm Demo"
    ui_params.root.handle.width = 1180
    ui_params.root.handle.height = 600
    # ui_params.root.handle.color.bg = '#555'
    ui_params.root.handle.gotoxy(80, 25)

    UI_LabelsDraw()
    sensor_readings=SENSOR_READINGS()
    # First time Make it True
    sensor_readings.tof1.available = True
    sensor_readings.tof2.available = True
    sensor_readings.ge.available = True
    sensor_readings.imu.available = True
    UI_GridEyeDraw(sensor_readings)
    UI_PRDraw(sensor_readings)
    UI_Tof1Draw(sensor_readings)
    UI_Tof2Draw(sensor_readings)
    UI_IMUDraw(sensor_readings)
    UI_Update_Thread()
    END(ui_params.root.handle)

def test():
    print("close")
    print("close")
    print("close")
    print("close")
    print("close")
    print("close")


class SerialMonitor:
    def __init__(self):
        self.sensor_readings = SENSOR_READINGS()
        self.handle = None
        self.available = False
        self.connected = False
        self.dummy_value = "  < Select Serial Port >  "
        self.serial_port_str = None
        self.baudrate = 9600

    def scan(self):
        global sport_list_str
        sport_list_raw = serial.tools.list_ports.comports()
        sport_list_str = [self.dummy_value]

        for element in sport_list_raw:
            sport_list_str.append(str(element.device))

        if len(sport_list_str) > 1:
            self.avaliable = True
        else:
            self.avaliable = False

        return sport_list_str

    def connect(self, serial_port_str, baud_rate):
        try:
            self.serial_port_str = serial_port_str
            self.baudrate = baud_rate
            #Serial Buffer Size
            self.handle = serial.Serial(self.serial_port_str, self.baudrate)
            #self.handle.ReadBufferSize=10000
            self.handle.flushInput()
        except:
            print("error: serial connect -> " + serial_port_str)
            self.connected = False
            return

        self.connected = True
        try:
            self.handle.flushInput()
        except:
            print("error: serial flush -> " + self.serial_port_str)
            self.connected = False

    def close(self):
        if self.connected == True:
            self.handle.close()
            self.connected = False

    def read(self):
        serial_data = None

        if self.connected == True:
            try:
                serial_data = self.handle.readline()
                #Serial Flush Before Read 
                #self.handle.flushInput()
            except:
                print("error: serial read -> " + self.serial_port_str)
                self.connected = False

        return serial_data

    def parse_pressure(self, serial_data):
        s = str(serial_data)
        if DEBUG == True: print("PR(1)-->" + str(s))
        e1 = s.split("PR#")
        if DEBUG == True: print("PR(2)-->" + str(e1))

        if (len(e1) >= 2):
            e2 = e1[1]
            if DEBUG == True: print("PR(3)-->" + str(e2))

            # pressure values are here
            e3 = e2.split(",")
            if DEBUG == True: print("PR(4)-->" + str(e3))

            if self.sensor_readings.pr.available != True:
                for i in range(0, 60):
                    self.sensor_readings.pr.pressure[i] = int(e3[i])

                self.sensor_readings.pr.available = True

                if DEBUG == True: print("PR(5)-->" + str(self.sensor_readings.pr.pressure))

    def parse_tof_ge_imu(self, serial_data):
        s = str(serial_data)
        
        if DEBUG == True: print("GE(1)-->" + str(s))
        e1 = s.split("GE#")
        if DEBUG == True: print("GE(2)-->" + str(e1))

        if (len(e1) >= 2):
            e2 = e1[1]
            if DEBUG == True: print("GE(3)-->" + str(e2))

            # grid eye pixel values are here
            e3 = e2.split(",")
            if DEBUG == True: print("GE(4)-->" + str(e3))

            if self.sensor_readings.ge.available != True:
                for i in range(0, 64):
                    self.sensor_readings.ge.pixel[i] = int(e3[i])

                self.sensor_readings.ge.available = True

                if DEBUG == True: print("GE(5)-->" + str(self.sensor_readings.ge.pixel))

        if DEBUG == True: print("TOF(1)-->" + str(s))
        e1 = s.split("TOF#")
        if DEBUG == True: print("TOF(2)-->" + str(e1))
        if (len(e1) >= 2):
            e2 = e1[1]
            if DEBUG == True: print("TOF(3)-->" + str(e2))
            # grid eye sensor_readings.ge.pixel values are here
            e3 = e2.split(",")
            if DEBUG == True: print("TOF(4)-->" + str(e3))
            if DEBUG == True: print("TOF(5) TOF1 READING -->" + str(e3[0]))
            if DEBUG == True: print("TOF(6) TOF2 READING -->" + str(e3[1]))

            if self.sensor_readings.tof1.available != True:
                self.sensor_readings.tof1.distance = int(float(e3[0]))
                self.sensor_readings.tof1.available = True

        if self.sensor_readings.tof2.available != True:
            self.sensor_readings.tof2.distance = int(float(e3[1]))
            self.sensor_readings.tof2.available = True

        if DEBUG == True: print("IMU(1)-->" + str(s))
        e1 = s.split("IMU#")
        if DEBUG == True: print("IMU(2)-->" + str(e1))
        if (len(e1) >= 2):
            e2 = e1[1]
            if DEBUG == True: print("IMU(3)-->" + str(e2))

            # values are here
            e3 = e2.split(",")
            if DEBUG == True: print("IMU(4)-->" + str(e3))
            if DEBUG == True: print("IMU(5.1)-->" + str(e3[0]))
            if DEBUG == True: print("IMU(5.2)-->" + str(e3[1]))
            if DEBUG == True: print("IMU(5.3)-->" + str(e3[2]))
            if DEBUG == True: print("IMU(5.4)-->" + str(e3[3]))

            if self.sensor_readings.imu.available != True:
                self.sensor_readings.imu.w = float(e3[0])
                self.sensor_readings.imu.x = float(e3[1])
                self.sensor_readings.imu.y = float(e3[2])
                self.sensor_readings.imu.z = float(e3[3])
                self.sensor_readings.imu.available = True
                self.sensor_readings.imu.generate_euler_angles()

    def parse(self, serial_data):
        s = str(serial_data)
        ret = s.find('PR#')
        if ret != -1:
            self.parse_pressure(serial_data)
    
        ret = s.find('GE#')
        if ret != -1:
            self.parse_tof_ge_imu(serial_data)

def UI_Update():
    global ui_params, sensor_queue
    if sensor_queue.empty():
        #print_frontend_latency()
        return
    
    try:
        sensor_readings = sensor_queue.get()
        UI_GridEyeDraw(sensor_readings)
        UI_PRDraw(sensor_readings)
        UI_Tof1Draw(sensor_readings)
        UI_Tof2Draw(sensor_readings)
        UI_IMUDraw(sensor_readings)
        ui_params.root.handle.update()
        sensor_queue.task_done()
    except:
        pass

    #print_frontend_latency()

def print_backend_latency():
    global tstart_be
    tend = datetime.now()
    tdiff = tend - tstart_be
    tdiff_ms = (tdiff.microseconds / 1000)
    print("BE : "+str(tdiff_ms))
    tstart_be=tend

def print_frontend_latency():
    global tstart_fe
    tend = datetime.now()
    tdiff = tend - tstart_fe
    tdiff_ms = (tdiff.microseconds / 1000)
    print("FE : "+str(tdiff_ms))
    tstart_fe=tend

def Backend_Routine():
    global sensor_queue
    global serial_port, baud_rate
    global serial_handle
    data_raw = None
    ui_update = True

    if serial_handle.connected == False:
        serial_handle.connect(serial_port, baud_rate)
        if serial_handle.connected == False:
            #print_backend_latency()
            return

    data_raw = serial_handle.read()
    serial_handle.parse(data_raw)
    sensor_queue.put(serial_handle.sensor_readings)
    #print_backend_latency()


def Backend_Loop():
    global program_quit
    global sensor_queue
    while True:
        
        if program_quit == True:
            return
            
        Backend_Routine()
        time.sleep(INTERVAL_BE)

def UI_Update_Loop():
    global program_quit
    while True:
        
        if program_quit == True:
            return
            
        UI_Update()
        time.sleep(INTERVAL_FE)

def UI_Update_Thread():
    global thread_ui
    thread_ui = threading.Thread(target=UI_Update_Loop)
    thread_ui.start()

def Backend_Routine_Thread():
    global thread_be
    thread_be = threading.Thread(target=Backend_Loop)
    thread_be.start()

def MAIN():
    global serial_handle, sensor_queue, thread_be, thread_ui, program_quit
    sensor_queue = queue.Queue()
    serial_handle = SerialMonitor()
    Backend_Routine_Thread()
    Frontend_Routine()
    print("close")
    program_quit=True
    
    try:
        thread_be.exit()
    except:
        pass

    try:
        thread_ui.exit()
    except:
        pass


    try:
        sensor_queue.join()
    except:
        pass


        
def ARG_CHECK():
    global serial_port, baud_rate
    serial_port = None
    baud_rate = None

    try:
        serial_port = sys.argv[1]
    except:
        pass

    try:
        baud_rate = sys.argv[2]
    except:
        pass

    if serial_port == None:
        print("error: feed serial port name as argument")
        exit(1)

    if baud_rate == None:
        print("error: feed baudrate name as argument")
        exit(1)

    print("Serial Port  : " + str(serial_port))
    print("Baud Rate    : " + str(baud_rate))


ARG_CHECK()
MAIN()
